#! /bin/bash
# 还原项目（下载依赖包）
dotnet restore
# 发布项目，将生成的文件保存到 bin/publish 文件夹 
dotnet publish -c Release -o bin/publish DockerCompose.Sample.Api.csproj
# 构建生成镜像，镜像名为 docker_compose.sample.api
# 这里将使用同目录下的 Dockfile
docker build -t docker_compose.sample.api .
# 创建一个容器 将宿主机的6000映射到容器的80端口
docker run -p 9100:80 --name mysampleapi --restart always  docker_compose.sample.api
