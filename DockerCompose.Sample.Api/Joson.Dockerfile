# FROM registry.cn-beijing.aliyuncs.com/lhtzbj12/dotnet_core_aspnet:3.1 AS base
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443

# FROM registry.cn-beijing.aliyuncs.com/lhtzbj12/dotnet_core_sdk:3.1 AS restore
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore
# RUN echo "准备COPY" && ls  && pwd
WORKDIR /src

RUN echo "COPY过程" && ls  && pwd
# When using COPY with more than one source file, the destination must be a directory and end with a /
COPY ["*/*.csproj","./"]
# # 错误的方式 no source files were specified
# COPY ["*.csproj","DockerCompose.Sample.Api/"]
# # 不推荐的方式
# COPY ["DockerCompose.Sample.Api/*.csproj","."]

RUN ls && pwd
RUN for file in $(ls *.csproj); \
    do mkdir -p ${file%.*}/  \
    && echo Restore项目文件：$file ; \
    echo Restore项目文件路径： $file ${file%.*} ; \
    prog=${file%.*}/$file ; \
    echo prog变量 $prog ; \
    mv $file ${file%.*}/ ; \
    done ; \
    ls  ; 

# COPY . .    

# RUN echo "Restore Build 编译过程"
# RUN ls && pwd
RUN echo "Restore Build 编译过程" && ls && pwd && \
    for file in $(ls); \
    do prog=$file/$file.csproj ; \
    echo 当前file目录：$file ; \
    echo 当前PWD目录：${PWD} $(pwd); \
    echo 编译项目文件路径：$prog ; \
    dotnet restore $prog ; \
    # 这里不能 编译 ？？ 因为还没有文件 Main 肯定没有啊
    # 需要先 Copy 文件 否则 CS5001 错误
    # dotnet build $prog -c Release -o /app/build ; \
    done ; \
    ls  

# # # -----------------------------构建过程不可取镜像中有垃圾------------------------------------
# 需要先 Copy . . 然后 Restore Build 同时进行
# FROM restore AS Api-publish
# WORKDIR "/src/DockerCompose.Sample.Api"
# RUN dotnet publish "DockerCompose.Sample.Api.csproj" -c Release -r linux-x64 -o /app/publish
# # # -----------------------------构建过程不可取镜像中有垃圾------------------------------------


# ---------------------------------正常构建过程---------------------------------------------

# 这里可以合并到上面构建镜像层中，需要先 Copy 文件 否则 CS5001 错误
# Restore 完毕仅有文件夹 obj 和 项目文件 .csproj ，需要拷贝其他文件到工作目录
# 否则 error CS5001: Program does not contain a static 'Main' method suitable for an entry point [/src/DockerCompose.Sample.Api/DockerCompose.Sample.Api.csproj]
RUN ls && pwd && echo 'WORKDIR 之后到此 之间的代码都是 无用的，用于调试帮助理解'
COPY . .
RUN ls && pwd

FROM restore AS Api-build
WORKDIR "/src/DockerCompose.Sample.Api"
RUN ls && dotnet build "DockerCompose.Sample.Api.csproj" -c Release -o /app/build


FROM Api-build AS Api-publish
RUN dotnet publish "DockerCompose.Sample.Api.csproj" -c Release -o /app/publish

# ---------------------------------正常构建过程----------------------------------------------



FROM base AS final
WORKDIR /srv/webapi
COPY --from=Api-publish /app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll","--urls=http://0.0.0.0:80"]
