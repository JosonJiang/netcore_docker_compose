#! /bin/bash
# 还原项目（下载依赖包）
dotnet restore
# 发布项目，将生成的文件保存到 bin/publish 文件夹 
dotnet publish -c Release -o bin/publish DockerCompose.Sample.api.csproj
# 构建生成镜像，镜像名为 docker_compose.sample.api
# 这里将使用同目录下的 Dockfile
# docker build -t docker_compose.sample.api -f Joson.Dockerfile .
docker build -t docker_compose_debug.sample.api -f DockerCompose.Sample.Api/Joson.Dockerfile . --no-cache
# 创建一个容器，将宿主机的6001映射到容器的80端口
docker rm  -f  debugapi
docker run -p 6001:80 --name debugapi  docker_compose_debug.sample.api