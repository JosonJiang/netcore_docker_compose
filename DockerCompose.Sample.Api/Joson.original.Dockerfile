FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443


FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore
RUN ls && pwd
WORKDIR /src
RUN ls && pwd
COPY [".","./"]
RUN ls && pwd && dotnet restore "/src/DockerCompose.Sample.Api" && \
    ls && pwd

# COPY ["*/*.csproj","./"]
# RUN ls && pwd && dotnet restore "/src/DockerCompose.Sample.Api"

FROM restore AS Api-build
WORKDIR "/src/DockerCompose.Sample.Api"
RUN dotnet build "DockerCompose.Sample.Api.csproj" -c Release -o /app/build

FROM Api-build AS Api-publish
RUN dotnet publish "DockerCompose.Sample.Api.csproj" -c Release -o /app/publish


FROM base AS final
WORKDIR /srv/webApi
COPY --from=Api-publish /app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll","--urls=http://0.0.0.0:80"]