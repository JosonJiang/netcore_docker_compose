﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DockerCompose.Sample.IService;

namespace DockerCompose.Sample.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<HealthController> _logger;
        IUserService _IUserService;

        public HealthController(ILogger<HealthController> logger ,IUserService iUserService)
        {
            _logger = logger;
            _IUserService=iUserService;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var Response = _IUserService.Say("JosonJiang", " Hello");
            Summaries.Append(Response);

            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                JosonJiang=Response,
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
