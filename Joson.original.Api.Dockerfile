FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore
WORKDIR /src
COPY [".","."]
# #调试信息部分
RUN pwd && ls
# #调试信息部分
RUN dotnet restore


FROM restore AS Api-build
WORKDIR "/src/DockerCompose.Sample.Api"
RUN dotnet build "DockerCompose.Sample.Api.csproj" -c Release -o /app/build

FROM Api-build AS Api-publish
RUN dotnet publish "DockerCompose.Sample.Api.csproj" -c Release -o /app/publish


FROM base AS final
WORKDIR /srv/webapi
COPY --from=Api-publish app/publish .
# #调试信息部分
RUN pwd && ls
# #调试信息部分
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll","--urls=http://0.0.0.0:80"]
