FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore
WORKDIR /src
COPY [".","."]
RUN dotnet restore 

FROM restore AS Api-build
WORKDIR "/src/DockerCompose.Sample.Api"
RUN dotnet build "DockerCompose.Sample.Api.csproj" -c Release -o /app/build

FROM Api-build AS Api-publish
RUN dotnet publish "DockerCompose.Sample.Api.csproj" -c Release -o /app/publish

FROM base AS Api
WORKDIR /WebApi
COPY --from=Api-publish /app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll"]

FROM restore AS Web-build
WORKDIR "/src/DockerCompose.Sample.Web"
RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build

FROM Web-build AS Web-publish
RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish

FROM base AS Web
WORKDIR /WebApp
COPY --from=Web-publish /app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll"]
