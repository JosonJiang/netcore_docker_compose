FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore
WORKDIR /src
COPY [".","."]
RUN dotnet restore 

FROM restore AS Web-build
WORKDIR "/src/DockerCompose.Sample.Web"
RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build

FROM Web-build AS Web-publish
RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish


FROM base AS final
WORKDIR /srv/webApp
COPY --from=Web-publish app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll","--urls=http://0.0.0.0:80"]
