apt-get update
apt-get install --yes openjdk-11-jre
dotnet tool install --global dotnet-sonarscanner
export PATH=\"$PATH:$HOME/.dotnet/tools\"
dotnet sonarscanner begin /k:\"JosonSonar\" /d:sonar.login=\"$SONAR_TOKEN\" /d:\"sonar.host.url=$SONAR_HOST_URL\"
dotnet build
dotnet sonarscanner end /d:sonar.login=\"$SONAR_TOKEN\"