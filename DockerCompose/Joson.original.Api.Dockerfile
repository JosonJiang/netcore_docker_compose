FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443

FROM netcore3.1_api AS baseImages
WORKDIR /srv/webApi
# 基础镜像 【netcore3.1_api】 的 WORKDIR 是 /srv/webapi，上面是句废话？？
# 拷贝【当前目录=Dockerfile所在目录！！！不是 From 镜像中的目录】到新的 镜像层的 src 文件夹下
# COPY [".","."]
RUN ls && pwd



FROM base as scratch

EXPOSE 8080
EXPOSE 8443
LABEL maintainer="JosonJiang"

WORKDIR /srv/JosonWebApi
RUN ls && pwd
COPY --from=baseImages /srv/webApi .
RUN ls && pwd 
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll","--urls=http://0.0.0.0:8080"]

# docker run -itd --name fromapibaseimg -p 8443:8080 docker_compose.sample.api_frombaseimg:3.0
