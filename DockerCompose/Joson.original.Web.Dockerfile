# FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
# EXPOSE 80
# EXPOSE 443

FROM netcore3.1_web AS baseImages
WORKDIR /srv/webApp

FROM baseImages as scratch
EXPOSE 8080
EXPOSE 8443
LABEL maintainer="JosonJiang"

WORKDIR /srv/JosonWebApp
COPY --from=baseImages /srv/webApp .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll","--urls=http://0.0.0.0:8080"]


# docker run -itd --name fromwebbaseimg -p 8080:8080 docker_compose.sample.web_frombaseimg:3.0
