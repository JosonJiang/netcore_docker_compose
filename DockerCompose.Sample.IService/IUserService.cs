﻿using System;

namespace DockerCompose.Sample.IService
{
    public interface IUserService
    {
        /// <summary>
        /// 说话
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="message">说话内容</param>
        /// <returns></returns>
        string Say(string username, string message);
    }
}
