# netcore_docker_compose

#### 介绍
.net core多项目解决方案里使用shell、docker、docker_compose进行构建、部署

- 一个解决方案里有多个项目
- 一个项目构建成一个镜像

# 本文提供三种部署方案

下文中的三种方案都需要在宿主机里安装docker。

## 1. 单个项目分开构建、部署

第一种方案依赖宿主机的dotnet开发环境。以DockerCompose.Sample.Api为，执行与子项目.csproj同级的publish.sh

```bash
cd DockerCompose.Sample.Api
# 赋予执行权限
chmod 777 ./publish.sh
# 执行
./publish.sh
```

publish.sh里有详细注释，进行进一步说明

执行结束后，可以访问 http://0.0.0.0:9100/health

补充：这种方式适合单个项目构建镜像、部署上线。

## 2. 整体构建、部署

第二种方案依赖宿主机的dotnet开发环境，且宿主机需要安装docker-compose。使用shell命令将整个解决方案restore，依次发布每个项目，然后使用docker-compose直接部署，而docker-compose会调用各个项目的Dockerfile构建镜像。

执行与.sln同级的publish.sh

```bash
# 赋予执行权限
chmod 777 ./publish.sh
# 将 docker-compose-shell.yml 命名为 docker-compose.yml
mv docker-compose-shell.yml docker-compose.yml
# 执行
./publish.sh
```

停止并删除docker-compose.yml所定义的容器

```bash
docker-compose down
```

注：这种方式比较适合在测试环境配合jenkins进行自动构建部署。

## 3. 纯Docker部署

第三方案需要在宿主里安装docker-compose，整个构建过程都在容器里进行。

```bash
# 将 docker-compose-all.yml 命名为 docker-compose.yml
cp docker-compose-all.yml docker-compose.yml
# 执行
docker-compose up --build -d
```

这种方式的优势是不依赖宿主的dotnet开发环境。
