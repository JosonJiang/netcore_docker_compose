FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443


FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS Joson_restore
WORKDIR /src
# COPY ["DockerCompose.Sample.sln", "./"]
# COPY ["*/*.csproj", "./"]
# RUN for file in $(ls *.csproj); do mkdir -p ${file%.*}/ && mv $file ${file%.*}/ ; done
COPY [".", "./"]
#调试信息部分
RUN pwd && ls
#调试信息部分
RUN dotnet restore


FROM Joson_restore AS Api-build
WORKDIR "/src/DockerCompose.Sample.Api"
# RUN dotnet restore "DockerCompose.Sample.Api.csproj"
RUN dotnet build "DockerCompose.Sample.Api.csproj" -c Release -o /web-api/build



FROM Joson_restore AS Web-build
WORKDIR "/src/DockerCompose.Sample.Web"
# RUN dotnet restore "DockerCompose.Sample.Web.csproj"
RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /web-app/build



FROM Api-build AS Api-publish
WORKDIR "/src/DockerCompose.Sample.Api"
RUN dotnet publish "DockerCompose.Sample.Api.csproj" -c Release -o /web-api/publish


FROM base AS Api_final
WORKDIR /srv/webapi
COPY --from=Api-publish web-api/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll","--urls=http://0.0.0.0:80"]



FROM Web-build AS Web-publish
WORKDIR "/src/DockerCompose.Sample.Web"
RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /web-app/publish


FROM base AS Web_final
WORKDIR /srv/webapp
COPY --from=Web-publish web-app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Api.dll","--urls=http://0.0.0.0:80"]

