﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DockerCompose.Sample.Web.Models;
using DockerCompose.Sample.IService;

namespace DockerCompose.Sample.Web.Controllers
{

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        IUserService _IUserService;

        public HomeController(ILogger<HomeController> logger, IUserService iUserService)
        {
            _logger = logger;
            _IUserService =iUserService;
        }

        public IActionResult Index()
        {
            var Response= _IUserService.Say("JosonJiang"," Hello");
            return View("~/Views/Home/Index.cshtml",Response);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
