FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443


FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore-build
# WORKDIR /src

# RUN echo "COPY 开始" && ls  && pwd
# # When using COPY with more than one source file, the destination must be a directory and end with a /
# COPY ["*/*.csproj","./"]
# # RUN dotnet restore
# RUN ls && pwd

# RUN for file in $(ls *.csproj); \
#     do mkdir -p ${file%.*}/ &&  \
#     # echo $file ${file%.*} && \
#     mv $file ${file%.*}/ && \
#     prog=${file%.*}/$file && \
#     echo $prog ; \
#     done ; \
#     ls  ; 

# WORKDIR "/src/DockerCompose.Sample.Web"
# RUN ls && pwd && dotnet restore "DockerCompose.Sample.Web.csproj" && ls && pwd 
# # RUN Restore 完毕后工作目录在 /src/DockerCompose.Sample.Web
# # 该目录下文件
# # DockerCompose.Sample.Web.csproj
# # obj 

# # 切换工作目录到 /src ， 拷贝项目中其他文件到 工作目录（src）  
# # Restore 后的项目文件 .csproj 和对应 obj 与 其他文件合并
# # 否则错误提示 CSC : error CS5001: Program does not contain a static 'Main' method suitable for an entry point [/src/DockerCompose.Sample.Web/DockerCompose.Sample.Web.csproj]

WORKDIR "/src"
RUN ls && pwd 
COPY [".","./"]
RUN ls && pwd

# 再次切换工作目录到 /src/DockerCompose.Sample.Web 供后面镜像层使用
WORKDIR "/src/DockerCompose.Sample.Web"
RUN ls && pwd 

# ---------------------------------减少镜像层构建过程---------------------------------------------

# # 可以独立出来一个镜像层
# RUN ls && pwd &&  dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build  && ls && pwd

# FROM restore-build AS Web-publish
# RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish
# # 可以独立出来一个镜像层

# ---------------------------------减少镜像层构建过程---------------------------------------------



# ---------------------------------正常构建过程---------------------------------------------

# 可以减少一个镜像层 合并到 Restore 过程中
FROM restore-build AS Web-build
RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build
# 可以减少一个镜像层 合并到 Restore 过程中

FROM Web-build AS Web-publish
RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish


# ---------------------------------正常构建过程---------------------------------------------


FROM base AS Web
WORKDIR /srv/app
COPY --from=Web-publish /app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll"]
