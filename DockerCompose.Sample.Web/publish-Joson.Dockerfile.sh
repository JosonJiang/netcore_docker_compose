#! /bin/bash
# 还原项目（下载依赖包）
dotnet restore
# 发布项目，将生成的文件保存到 bin/publish 文件夹 
dotnet publish -c Release -o bin/publish DockerCompose.Sample.web.csproj
# 构建生成镜像，镜像名为 docker_compose.sample.web
# 这里将使用同目录下的 Dockfile
# docker build -t docker_compose.sample.web -f DockerCompose.Sample.Web/Joson.Dockerfile .
docker build -t docker_compose_debug.sample.web -f DockerCompose.Sample.Web/Joson.Dockerfile . --no-cache
# 创建一个容器，将宿主机的6002映射到容器的80端口
docker rm  -f  debugweb
docker run -p 6002:80 --name debugweb  docker_compose_debug.sample.web
