FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
EXPOSE 80
EXPOSE 443

# ==============================正常的构建=======================================

# FROM registry.cn-beijing.aliyuncs.com/lhtzbj12/dotnet_core_sdk:3.1 AS restore
# WORKDIR /src
# COPY [".","."]
# RUN dotnet restore 

# FROM restore AS Web-build
# WORKDIR "/src/DockerCompose.Sample.Web"
# RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build

# FROM Web-build AS Web-publish
# RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish

# FROM base AS Web
# WORKDIR /app
# COPY --from=Web-publish /app/publish .
# ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll"]

# ==============================正常的构建=======================================



# FROM registry.cn-beijing.aliyuncs.com/lhtzbj12/dotnet_core_sdk:3.1 AS Restore-build
# WORKDIR /src
# COPY [".","./"]
# # WORKDIR "/src/DockerCompose.Sample.Web"
# RUN ls && pwd && dotnet restore "DockerCompose.Sample.Web.csproj" && \
#     ls && pwd

# FROM Restore-build AS Web-build
# # WORKDIR "/src/DockerCompose.Sample.Web"
# RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build

# FROM Web-build AS Web-publish
# RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish

# FROM base AS Web
# WORKDIR /srv/app
# COPY --from=Web-publish /app/publish .
# ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll"]







FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS restore-build
RUN ls && pwd
WORKDIR /src
RUN ls && pwd
COPY [".","./"]
RUN ls && pwd
WORKDIR "/src/DockerCompose.Sample.Web"
RUN ls && pwd && dotnet restore "DockerCompose.Sample.Web.csproj" && \
    ls && pwd

# FROM restore AS Web-build
# WORKDIR "/src/DockerCompose.Sample.Web"
RUN dotnet build "DockerCompose.Sample.Web.csproj" -c Release -o /app/build

# FROM restore-build AS Web-publish
RUN dotnet publish "DockerCompose.Sample.Web.csproj" -c Release -o /app/publish


FROM base AS Web
WORKDIR /srv/webApp
COPY --from=restore-build /app/publish .
# COPY --from=Web-publish /app/publish .
ENTRYPOINT ["dotnet", "DockerCompose.Sample.Web.dll"]
