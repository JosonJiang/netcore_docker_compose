using DockerCompose.Sample.IService;
using System;

namespace DockerCompose.Sample.Service
{
    public class UserService : IUserService
    {
        public string Say(string username, string message)
        {
            return $"{username} says: {message}";
        }
    }
}

