#! /bin/bash
# 执行这个shell进行部署
# 执行过程
# 1. 还原解决方案
# 2. 发布Web站点至文件夹 bin/publish
# 3. 发布Api站点至文件夹 bin/publish
# 4. 执行docker-compose
# 5. docker-compose里会根据各个站点下的Dockerfile打包docker镜像
# 还原整个解决方案
dotnet restore
# 发布Web
cd DockerCompose.Sample.Web
dotnet publish -c Release -o bin/publish DockerCompose.Sample.Web.csproj
# 发布Api
cd ..
cd DockerCompose.Sample.Api
dotnet publish -c Release -o bin/publish DockerCompose.Sample.Api.csproj
cd ..
# 执行docker-compose
docker-compose up --build -d